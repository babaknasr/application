#!/bin/bash
config_file=$1
configmap_file=$2
counter=""
content=`cat $configmap_file`
line_number=`grep -no "******" $configmap_file | awk -F: '{print $1}'`
readarray -t lines_array <<< "$content"

var=`echo "${lines_array[$((line_number - 1))]}"`

for ((i = 0; i < ${#var}; i++));
do
    char="${var:i:1}"
    if [ "$char" == " " ];then 
		counter=$counter" " ;
	fi
done
echo $counter
#################################################
variables=`cat $config_file`
readarray -t variables_lines <<< "$variables"
variables_res=""
for line in "${variables_lines[@]}"; do
	variables_res="${variables_res}${counter}${line}"$'\n'
done

res=""
i=1
for line in "${lines_array[@]}"; do
	if [ $i == $line_number ];then 
		res="${res}${variables_res}"$'\n'
	    ((i++))
		continue
	fi
	res="${res}${line}"$'\n'
	((i++))
done
echo "$res" > $configmap_file
